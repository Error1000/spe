#ifndef UNITS_H_INCLUDED
#define UNITS_H_INCLUDED

#include <ostream>
#include <string>
#include <cmath>
#include <unordered_map>
#include <functional>
#include "CustomFloat.h"
#include "Scalar.h"

namespace SPE{

//IS = International System
namespace IS{


//Forward declarations
struct ISU;
std::ostream& operator<<(std::ostream& os, const ISU& s) noexcept;


//Use float to create a complicated hack for convenience
//ISU = International System Unit
struct ISU{
 private:
    typedef Float T;
    T val;

    ISU(T arg):
        val(arg){}
 public:

   template<typename OT> inline ISU operator*(const OT& other) const noexcept{ return ISU(this->val * other); }
   template<typename OT> inline ISU operator/(const OT& other) const noexcept{ return ISU(this->val / other); }

   inline ISU operator*(const ISU& other) const noexcept{ return ISU(this->val * other.val); }
   inline ISU operator/(const ISU& other) const noexcept{ return ISU(this->val / other.val); }
   inline bool operator==(const ISU& other) const noexcept{ return (this->val == other.val); }
   inline bool operator!=(const ISU& other) const noexcept{ return (this->val != other.val); }

   ISU operator~() const noexcept; // defined in the source file

   friend std::ostream& operator<<(std::ostream& os, const ISU& s) noexcept;


   friend struct SU;
   friend class ISUHasher;

};

class ISUHasher{
public:
    inline std::size_t operator()(const ISU& isu) const{
        return round( isu.val.getValue() * ACCURACY ) / ACCURACY;
    }

};


struct SU{


static const ISU G;
static const ISU M;
static const ISU S;
static const ISU N;


static const ISU KG;
static const ISU HG;
static const ISU DAG;

static const ISU DG;
static const ISU CG;
static const ISU MG;



static const ISU KM;
static const ISU HM;
static const ISU DAM;

static const ISU DM;
static const ISU CM;
static const ISU MM;



static const ISU KN;
static const ISU HN;
static const ISU DAN;

static const ISU DN;
static const ISU CN;
static const ISU MN;


static const ISU M_SQUARED;

static const ISU M_CUBED;

static const ISU S_SQUARED;

static const ISU Pa;



static const std::unordered_map<ISU, std::string, ISUHasher> names;

};


typedef SPE::Scalar<ISU, Float> Scalar;

}

}


#endif // UNITS_H_INCLUDED
