#ifndef SCALAR_H_INCLUDED
#define SCALAR_H_INCLUDED
#include <stdexcept>
#include <ostream>
#include <type_traits>

namespace SPE{

template<typename UT,
         typename QT>
struct Scalar{
    UT unit;
    QT quantity;

    explicit Scalar(const QT& q, const UT& u):
    unit(u), quantity(q){static_assert(!std::is_same<QT, UT>::value, "Can't have a scalar that has a unit and a quantity of the same type!");}

    inline Scalar<UT, QT> operator*(const Scalar<UT, QT>& other) const noexcept{ return Scalar<UT, QT>(this->quantity * other.quantity, (this->unit * other.unit)); }

    inline Scalar<UT, QT> operator/(const Scalar<UT, QT>& other) const noexcept{ return Scalar<UT, QT>(this->quantity / other.quantity, (this->unit / other.unit)); }

    inline Scalar<UT, QT> operator+(const Scalar<UT, QT>& other) const{
        if(this->unit != other.unit) throw std::invalid_argument("Unit mismatch when adding!");
        return Scalar<UT, QT>(this->quantity + other.quantity, this->unit);
    }

    inline Scalar<UT, QT> operator-(const Scalar<UT, QT>& other) const{
         if(this->unit != other.unit) throw std::invalid_argument("Unit mismatch when subtracting!");
         return Scalar<UT, QT>(this->quantity - other.quantity, this->unit);
    }

    inline bool operator==(const Scalar& other) const noexcept{
        return ((this->quantity == other.quantity) &&
                (this->unit == other.unit));
    }

    inline bool operator!=(const Scalar& other) const noexcept{
        return ((this->quantity != other.quantity) ||
                (this->unit != other.unit));
    }

    inline bool operator>(const Scalar& other) const{
        if(this->unit != other.unit) throw std::invalid_argument("Unit mismatch when compareing!");
        return (this->quantity > other.quantity);
    }

    inline bool operator<(const Scalar& other) const{
        if(this->unit != other.unit) throw std::invalid_argument("Unit mismatch when compareing!");
        return (this->quantity < other.quantity);
    }


    inline Scalar<UT, QT> operator*(const UT& other) const { return Scalar<UT, QT>(this->quantity, (this->unit * other)); }

    inline Scalar<UT, QT> operator/(const UT& other) const { return Scalar<UT, QT>(this->quantity, (this->unit / other)); }


    inline friend std::ostream& operator<<(std::ostream& os, const Scalar& s){
      // We assume that both the UT and QT are printable
      os << s.quantity << ' ' << s.unit;
      return os;
    }

};


}

#endif // SCALAR_H_INCLUDED
