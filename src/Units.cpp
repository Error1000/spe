#include "Units.h"


using namespace SPE::IS;

//NOTE: 7,11 and 13 were all choosen because their exponents never intersecet, they are prime, and you can multply one with x to get to the other if you are using floating point arhetmetic or even maybe double in c++, so they act like a nice base
const ISU SU::G(7);
const ISU SU::M(11);
const ISU SU::S(13);



const ISU SU::KG(SU::G*1000);
const ISU SU::HG(SU::G*100);
const ISU SU::DAG(SU::G*10);

const ISU SU::DG(SU::G*0.1);
const ISU SU::CG(SU::G*0.01);
const ISU SU::MG(SU::G*0.001);



const ISU SU::KM(SU::M*1000);
const ISU SU::HM(SU::M*100);
const ISU SU::DAM(SU::M*10);

const ISU SU::DM(SU::M*0.1);
const ISU SU::CM(SU::M*0.01);
const ISU SU::MM(SU::M*0.001);


const ISU SU::KN(SU::N*1000);
const ISU SU::HN(SU::N*100);
const ISU SU::DAN(SU::N*10);

const ISU SU::DN(SU::N*0.1);
const ISU SU::CN(SU::N*0.01);
const ISU SU::MN(SU::N*0.001);

const ISU SU::M_SQUARED(SU::M * SU::M);
const ISU SU::M_CUBED(SU::M_SQUARED * SU::M);
const ISU SU::S_SQUARED(SU::S * SU::S);
const ISU SU::N(SU::KG * (SU::M / SU::S_SQUARED));
const ISU SU::Pa(SU::N / SU::M_SQUARED);

const std::unordered_map<ISU, std::string, ISUHasher> SU::names ={
    {SU::KG, "kg"},   {SU::KM, "km"},    {SU::KN, "kN"},
    {SU::HG, "hg"},   {SU::HM, "hm"},    {SU::HN, "hN"},
    {SU::DAG, "dag"}, {SU::DAM, "dam"},  {SU::DAN, "daN"},
    {SU::G, "g"},     {SU::M, "m"},      {SU::N, "N"},       {SU::Pa, "Pa"},    {SU::M_SQUARED, "m^2"},    {SU::M_CUBED, "m^3"},   {SU::S, "s"}, {SU::S_SQUARED, "s^2"},
    {SU::DG, "dg"},   {SU::DM, "dm"},    {SU::DN, "dN"},
    {SU::CG, "cg"},   {SU::CM, "cm"},    {SU::CN, "cN"},
    {SU::MG, "mg"},   {SU::MM, "mm"},    {SU::MN, "mN"},
    {SU::M/SU::S_SQUARED, "m/s^2"},
};

// NOTE: We use IS:: here because namespace usings do not apply to friend functions
std::ostream& SPE::IS::operator<<(std::ostream& os, const ISU& s) noexcept{
      try{
         os << (SU::names.at(s));
      }catch(const std::out_of_range& OutOfRangeError){
         os << "?";
      }

      return os;
}

//Definition of ISU::operator~
ISU ISU::operator~() const noexcept{
    return ISU(1);
}

