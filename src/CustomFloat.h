#ifndef CUSTOMFLOAT_H_INCLUDED
#define CUSTOMFLOAT_H_INCLUDED

#define ACCURACY 100000.0 //decimal points to comapre(5)(10^5), because yay for floating point arithmetic

namespace SPE{

struct Float{
    public:
        typedef long double T;

        Float(const T& v):
        val(v){}

        inline Float operator*(const Float& other) const noexcept{return Float(this->val * other.val);}
        inline Float operator/(const Float& other) const noexcept{return Float(this->val / other.val);}
        inline Float operator+(const Float& other) const noexcept{return Float(this->val + other.val);}
        inline Float operator-(const Float& other) const noexcept{return Float(this->val - other.val);}

        //Yay for floating point arithmetic
        inline bool operator>(const Float& other) const noexcept{return (this->val > other.val);}
        inline bool operator<(const Float& other) const noexcept{return (this->val < other.val);}
        inline bool operator==(const Float& other) const noexcept{return ((this->val + (1/ACCURACY) > other.val) && (this->val - (1/ACCURACY) < other.val));}
        inline bool operator!=(const Float& other) const noexcept{return ((this->val + (1/ACCURACY) < other.val) || (this->val - (1/ACCURACY) > other.val));}
        inline const T getValue() const noexcept{ return this->val; }

        inline friend std::ostream& operator<<(std::ostream& os, const Float& f) noexcept{
            os << f.getValue();
            return os;
        }
    private:
        T val;
};

}

#endif // CUSTOMFLOAT_H_INCLUDED
