#include <iostream>
#include <iomanip>
#include "Scalar.h"
#include "Units.h"
#include <chrono>

using namespace SPE;
using namespace IS;



int main(){
    const IS::Scalar g(9.80665, SU::N / SU::KG); // gravitational acceleration
    const IS::Scalar m(90, SU::KG); // mass
    const auto now = std::chrono::system_clock::now();
    const IS::Scalar G(m * g); // calculate gravity
    const auto after = std::chrono::system_clock::now();
    const auto duration = after - now;
    std::cout << "Calculation of the gravity of an object with mass of: " << m << ", and using a gravitational acceleration of: " << g << " finished!" << '\n';
    std::cout << "The result was: " << G << ", and it took: " << std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count() << " nanoseconds, to calculate!";
}
