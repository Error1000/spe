# SPE ( "Simple" Physics Engine )

Note: This git repository uses submodules so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!

Note: Since this repository uses submodules DO NOT download it using the web interface it does not download submodules instead clone the project with the arguments: --recurse-submodules -j8 !!!

# Build library

 # GNU/Linux:
  Note: This command will download and install the required applications and libraries needed to build.
   Note: The automatron commands must be run from the project folder where this README is located!!!

   - Ubuntu: ./automatron9000/automatron9000.sh -m release -o ubuntu

   - Arch: ./automatron9000/automatron9000.sh -m release -o arch

 # MacOS ( using make to build and brew package manager to get libraries and install stuff ):
  Note: This command will download and install the required applications needed to build and install brew.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - ./automatron9000/automatron9000.sh -m release -o macos

 # Windows ( using make to build and the chocolatey package manager to install stuff, using the libraries from the Libraries\windows folder provided ):
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - Open a cmd as administrator and run: automatron9000\automatron9000.bat -m release -o windows



# Build debug

 # GNU/Linux:
  Note: This command will download and install the required applications and libraries needed to build.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - Ubuntu: ./automatron9000/automatron9000.sh -m debug -o ubuntu

   - Arch: ./automatron9000/automatron9000.sh -m debug -o arch

 # MacOS ( using make to build and brew package manager to get libraries and install stuff ):
  Note: This command will download and install the required applications needed to build and install brew.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - ./automatron9000/automatron9000.sh -m debug -o macos

 # Windows ( using make to build and the chocolatey package manager to install stuff, using the libraries from the Libraries\windows folder provided ):
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - Open a cmd as administrator and run: automatron9000\automatron9000.bat -m debug -o windows

